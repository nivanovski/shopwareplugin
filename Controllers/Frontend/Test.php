<?php


class Shopware_Controllers_Frontend_Test extends \Enlight_Controller_Action
{
    public function preDispatch()
    {
        /** @var \Shopware\Components\Plugin $plugin */
        $plugin = $this->get('kernel')->getPlugins()['DNHTest'];

        $this->get('template')->addTemplateDir($plugin->getPath() . '/Resources/views/');
    }

    public function indexAction()
    {
        var_dump('Custom controller index action');
        exit;
    }

    public function testAction()
    {
        $this->get('dnh_test.printer');
    }
}