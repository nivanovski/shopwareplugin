<?php

namespace DNHTest;

use DNHTest\Models\Settings;
use Shopware\Components\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Doctrine\ORM\Tools\SchemaTool;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;

class DNHTest extends Plugin
{
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('dnh_test.plugin_dir', $this->getPath());

        parent::build($container);
    }

    /**
     * @inheritdoc
     */
    public function install(InstallContext $context)
    {
        $this->createSchema();

        parent::install($context);
    }

    public function update(UpdateContext $context)
    {
        $this->updateSchema();

        parent::update($context);
    }

    public function uninstall(UpdateContext $context)
    {
        $this->uninstallSchema();

        parent::uninstall($context);
    }

    /**
     * creates database tables on base of doctrine models
     */
    private function createSchema()
    {
        $tool = new SchemaTool($this->container->get('models'));
        $classes = [
            $this->container->get('models')->getClassMetadata(Settings::class)
        ];
        $tool->createSchema($classes);
    }

    private function updateSchema()
    {
        $tool = new SchemaTool($this->container->get('models'));
        $classes = [
            $this->container->get('models')->getClassMetadata(Settings::class)
        ];
        $tool->updateSchema($classes, true);
    }

    private function uninstallSchema()
    {
        $tool = new SchemaTool($this->container->get('models'));
        $classes = [
            $this->container->get('models')->getClassMetadata(Settings::class)
        ];
        $tool->dropSchema($classes);
    }
}