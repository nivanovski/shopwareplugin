<?php

namespace DNHTest\Subscriber;

use Enlight\Event\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Controller implements SubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Dispatcher_ControllerPath_Frontend_Test' => 'registerController',
//            'Enlight_Controller_Dispatcher_ControllerPath_Backend_Test' => 'registerController'
        ];
    }

    public function registerController(\Enlight_Event_EventArgs $args)
    {
        $this->container->get('Template')->addTemplateDir(
            $this->getPluginPath() . '/Resources/views/'
        );

        switch ($args->getName()) {
            case 'Enlight_Controller_Dispatcher_ControllerPath_Frontend_Test':
                return $this->getPluginPath() . '/Controllers/Frontend/Test.php';
            case 'Enlight_Controller_Dispatcher_ControllerPath_Backend_Test':
                return $this->getPluginPath() . '/Controllers/Backend/Test.php';
        }
    }

    /**
     * @return string
     */
    private function getPluginPath()
    {
        return $this->container->getParameter('dnh_test.plugin_dir');
    }
}