<?php

namespace DNHTest\Subscriber;

use Enlight\Event\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Frontend implements SubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Frontend' => 'onFrontend'
        ];
    }


    public function onFrontend(\Enlight_Controller_ActionEventArgs $args)
    {
        $args->getSubject()->get('Template')->addTemplateDir(
            $this->getPluginPath() . '/Resources/views/'
        );
    }

    /**
     * @return string
     */
    private function getPluginPath()
    {
        return $this->container->getParameter('dnh_test.plugin_dir');
    }
}